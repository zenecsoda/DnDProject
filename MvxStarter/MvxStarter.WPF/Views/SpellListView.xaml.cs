﻿using MvvmCross.Platforms.Wpf.Views;

namespace MvxStarter.WPF.Views
{
    /// <summary>
    /// Interaction logic for SpellListView.xaml
    /// </summary>
    public partial class SpellListView : MvxWpfView
    {
        public SpellListView()
        {
            InitializeComponent();
        }
    }
}
