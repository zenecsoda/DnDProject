﻿using MvvmCross.Platforms.Wpf.Views;

namespace MvxStarter.WPF.Views
{
    /// <summary>
    /// Interaction logic for CreateNewSpellView.xaml
    /// </summary>
    public partial class CreateNewSpellView : MvxWpfView
    {
        public CreateNewSpellView()
        {
            InitializeComponent();
        }
    }
}
