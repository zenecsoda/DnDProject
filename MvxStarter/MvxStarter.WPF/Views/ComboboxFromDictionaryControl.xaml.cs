﻿using MvxStarter.Core.Models.Enums;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace MvxStarter.WPF.Views
{
    /// <summary>
    /// Interaction logic for ComboboxFromDictionary.xaml
    /// </summary>
    public partial class ComboboxFromDictionaryControl : UserControl, INotifyPropertyChanged
    {
        public ComboboxFromDictionaryControl()
        {
            InitializeComponent();
            ProficiencyListGrid.DataContext = this;
        }
        public Dictionary<ProficiencyType, IEnumerable<string>> ProficiencyDictionary2
        {
            get { return (Dictionary<ProficiencyType, IEnumerable<string>>)GetValue(ProficiencyDictionaryProperty); }
            set { SetValue(ProficiencyDictionaryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WordsList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ProficiencyDictionaryProperty =
            DependencyProperty.Register(nameof(ProficiencyDictionary2), typeof(Dictionary<ProficiencyType, IEnumerable<string>>), typeof(ComboboxFromDictionaryControl), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public ProficiencyType SelectedProficiencyType
        {
            get { return (ProficiencyType)GetValue(SelectedProficiencyTypeProperty); }
            set { SetValue(SelectedProficiencyTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WordsList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedProficiencyTypeProperty =
            DependencyProperty.Register(nameof(SelectedProficiencyType), typeof(ProficiencyType), typeof(ComboboxFromDictionaryControl), new FrameworkPropertyMetadata(ProficiencyType.NOT_SET, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string SelectedProficiency
        {
            get { return (string)GetValue(SelectedProficiencyProperty); }
            set { SetValue(SelectedProficiencyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WordsList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedProficiencyProperty =
            DependencyProperty.Register(nameof(SelectedProficiency), typeof(string), typeof(ComboboxFromDictionaryControl), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnPropertyChanged(nameof(CurrentProficiencyItems));
        }

        public IEnumerable<string> CurrentProficiencyItems
        {
            get
            {
                if (SelectedProficiencyType == ProficiencyType.NOT_SET)
                {
                    return new List<string>();
                }
                return ProficiencyDictionary2?[SelectedProficiencyType];
            }
        }

        //public IEnumerable<string> CurrentProficiencyType
        //{
        //    get => ProficiencyDictionary[SelectedProficiencyType];
        //    set =>
        //}
    }
}
