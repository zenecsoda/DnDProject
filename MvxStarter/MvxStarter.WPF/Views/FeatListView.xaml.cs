﻿using MvvmCross.Platforms.Wpf.Views;

namespace MvxStarter.WPF.Views
{
    /// <summary>
    /// Interaction logic for FeatListView.xaml
    /// </summary>
    public partial class FeatListView : MvxWpfView
    {
        public FeatListView()
        {
            InitializeComponent();
        }
    }
}
