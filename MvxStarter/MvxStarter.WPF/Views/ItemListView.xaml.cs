﻿using MvvmCross.Platforms.Wpf.Views;

namespace MvxStarter.WPF.Views
{
    /// <summary>
    /// Interaction logic for ItemListView.xaml
    /// </summary>
    public partial class ItemListView : MvxWpfView
    {
        public ItemListView()
        {
            InitializeComponent();
        }
    }
}
