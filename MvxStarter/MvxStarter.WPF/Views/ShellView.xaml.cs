﻿using MvvmCross.Platforms.Wpf.Views;

namespace MvxStarter.WPF.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : MvxWpfView
    {
        public ShellView()
        {
            InitializeComponent();
        }
    }
}