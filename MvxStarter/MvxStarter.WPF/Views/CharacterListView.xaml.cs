﻿using MvvmCross.Platforms.Wpf.Views;

namespace MvxStarter.WPF.Views
{
    /// <summary>
    /// Interaction logic for CharacterListView.xaml
    /// </summary>
    public partial class CharacterListView : MvxWpfView
    {
        public CharacterListView()
        {
            InitializeComponent();
        }
    }
}
