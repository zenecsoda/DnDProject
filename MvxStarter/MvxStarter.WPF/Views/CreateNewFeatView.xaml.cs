﻿using MvvmCross.Platforms.Wpf.Views;

namespace MvxStarter.WPF.Views
{
    /// <summary>
    /// Interaction logic for CreateNewFeatView.xaml
    /// </summary>
    public partial class CreateNewFeatView : MvxWpfView

    {
        public CreateNewFeatView()
        {
            InitializeComponent();
        }
    }
}
