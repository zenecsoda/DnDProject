﻿using MvxStarter.Core.Models.BackGrounds;
using MvxStarter.Core.Models.Classes;
using MvxStarter.Core.Models.Feats;
using MvxStarter.Core.Models.Items;
using MvxStarter.Core.Models.PC;
using MvxStarter.Core.Models.Races;
using MvxStarter.Core.Models.Spells;
using MvxStarter.DataAccess.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MvxStarter.DataAccess.ContextModels
{
    public class ContextModel : IContextModel
    {
        public string FileFolderPath { get; set; }

        public string FileName { get; set; }

        public string FilePath => $"{FileFolderPath}\\{FileName}.DnDFantasyName.json";
        
        [JsonProperty]
        public Repository<IPC> CharacterRepository { get; private set; }
        [JsonProperty]
        public Repository<IClass> ClassRepository { get; private set; }
        [JsonProperty]
        public Repository<IRace> RaceRepository { get; private set; }
        [JsonProperty]
        public Repository<ISpell> SpellRepository { get; private set; }
        [JsonProperty]
        public Repository<IItem> ItemRepository { get; private set; }
        [JsonProperty]
        public Repository<IFeat> FeatRepository { get; private set; }
        [JsonProperty]
        public Repository<IBackGround> BackGroundRepository { get; private set; }

        public ContextModel()
        {

        }

        public ContextModel(string fileFolderPath,
                            string fileName,
                            Repository<IPC> characterRepository,
                            Repository<IClass> classRepository,
                            Repository<IRace> raceRepository,
                            Repository<ISpell> spellRepository,
                            Repository<IItem> itemRepository,
                            Repository<IFeat> featRepository,
                            Repository<IBackGround> backGroundRepository)
        {
            FileFolderPath = fileFolderPath;
            FileName = fileName;
            CharacterRepository = characterRepository;
            ClassRepository = classRepository;
            RaceRepository = raceRepository;
            SpellRepository = spellRepository;
            ItemRepository = itemRepository;
            FeatRepository = featRepository;
            BackGroundRepository = backGroundRepository;
        }

        public void InitializeNewContextModel(string fileFolderPath)
        {
            FileFolderPath = fileFolderPath;
            CharacterRepository = new Repository<IPC>(new List<IPC>());
            ClassRepository = new Repository<IClass>(new List<IClass>());
            RaceRepository = new Repository<IRace>(new List<IRace>());
            SpellRepository = new Repository<ISpell>(new List<ISpell>());
            ItemRepository = new Repository<IItem>(new List<IItem>());
            FeatRepository = new Repository<IFeat>(new List<IFeat>());
            BackGroundRepository = new Repository<IBackGround>(new List<IBackGround>());
        }

        public void SaveChanges()
        {
            //SERIALIZE
            string JSONresult = JsonConvert.SerializeObject(this, Formatting.Indented);

            //DELETE TO OVERRIDE
            if (File.Exists(FilePath))
            {
                File.Delete(FilePath);
            }

            //WRITE
            using (var streamWriter = new StreamWriter(FilePath, true))
            {
                streamWriter.WriteLine(JSONresult.ToString());
                streamWriter.Close();
            }
        }

        public void OpenFile()
        {
            string JSONfromFile;
            using (var reader = new StreamReader(FilePath))
            {
                JSONfromFile = reader.ReadToEnd();
            }

            var contextModel = JsonConvert.DeserializeObject<ContextModel>(JSONfromFile);

            CharacterRepository = contextModel.CharacterRepository;
            ClassRepository = contextModel.ClassRepository;
            RaceRepository = contextModel.RaceRepository;
            SpellRepository = contextModel.SpellRepository;
            ItemRepository = contextModel.ItemRepository;
            FeatRepository = contextModel.FeatRepository;
            BackGroundRepository = contextModel.BackGroundRepository;
        }
    }
}
