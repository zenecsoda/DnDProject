﻿using MvxStarter.Core.Models.Enums;
using System.Collections.Generic;

namespace MvxStarter.Core.Services
{
    public static class GetProficienciesService
    {
        public static Dictionary<ProficiencyType, IEnumerable<string>> FillProficiencyList()
        {
            var proficiencies = new Dictionary<ProficiencyType, IEnumerable<string>>
            {
                { ProficiencyType.NOT_SET, new List<string>() },
                { ProficiencyType.Skill, GetSkills() },
                { ProficiencyType.Weapon, GetWeaponProficiencies() },
                { ProficiencyType.Armor, GetArmorProficiencies() },
                { ProficiencyType.Tool, GetToolProficiencies() },
                { ProficiencyType.Language, GetLanguageProficiencies() }
            };
            return proficiencies;
        }

        private static IEnumerable<string> GetSkills()
        {
            var skillList = new List<string>
            {
                "Acrobatics",
                "Animal handling",
                "Arcana",
                "Athletics",
                "Deception",
                "History",
                "Insight",
                "Intimidation",
                "Investigation",
                "Medicine",
                "Nature",
                "Perception",
                "Performance",
                "Persuasion",
                "Religion",
                "Sleight of Hand",
                "Stealth",
                "Survival"
            };

            return skillList;
        }

        private static IEnumerable<string> GetWeaponProficiencies()
        {
            var weaponList = new List<string>
            {

                "Battleaxe",
                "Blowgun",
                "Club",
                "Crossbow, hand",
                "Crossbow, heavy",
                "Crossbow, light",
                "Dagger",
                "Dart",
                "Flail",
                "Glaive",
                "Greataxe",
                "Greatclub",
                "Greatsword",
                "Halberd",
                "Handaxe",
                "Javelin",
                "Lance",
                "Light Hammer",
                "Longbow",
                "Longsword",
                "Mace",
                "Martial Melee Weapons",
                "Martial Ranged Weapons",
                "Maul",
                "Morningstar",
                "Net",
                "Pike",
                "Quarterstaff",
                "Rapier",
                "Scimitar",
                "Shortbow",
                "Shortsword",
                "Sickle",
                "Simple Ranged Weapons",
                "Sling",
                "Spear",
                "Trident",
                "War Pick",
                "Warhammer",
                "Whip"
            };

            return weaponList;
        }

        private static IEnumerable<string> GetArmorProficiencies()
        {
            var armorList = new List<string>
            {
                "Light Armor",
                "Medium Armor",
                "Heavy Armor",
                "Shield"
            };

            return armorList;
        }

        private static IEnumerable<string> GetToolProficiencies()
        {
            var toolList = new List<string>
            {
                "Alchemist’s supplies",
                "Bagpipes",
                "Brewer’s supplies",
                "Calligrapher's Supplies",
                "Carpenter’s tools",
                "Cartographer’s tools",
                "Cobbler’s tools",
                "Cook’s utensils",
                "Dice set",
                "Drum",
                "Dulcimer",
                "Flute",
                "Gaming Sets",
                "Glassblower’s tools",
                "Horn",
                "Jeweler’s tools",
                "Leatherworker’s tools",
                "Lute",
                "Lyre",
                "Mason’s tools",
                "Musical Instruments",
                "Navigator’s tools",
                "Painter’s supplies",
                "Pan flute",
                "Playing card set",
                "Potter’s tools",
                "Shawm",
                "Smith’s tools",
                "Thieves’ tools",
                "Tinker’s tools",
                "Vehicles (land or water)",
                "Viol",
                "Weaver’s tools",
                "Woodcarver’s tools"
            };
            return toolList;
        }

        private static IEnumerable<string> GetLanguageProficiencies()
        {
            var languageList = new List<string>
            {
                "Abyssal",
                "Celestial",
                "Common",
                "Deep Speech",
                "Draconic",
                "Dwarvish",
                "Elvish",
                "Giant",
                "Gnomish",
                "Goblin",
                "Halfling",
                "Infernal",
                "Orc",
                "Primordial",
                "Sylvan",
                "Undercommon"

            };

            return languageList;
        }
    }
}
