﻿using MvvmCross;
using MvvmCross.ViewModels;
using MvxStarter.Core.ContextModels;
using MvxStarter.Core.ViewModels;

namespace MvxStarter.Core
{
    public class App : MvxApplication<IContextModel>
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.RegisterType<IContextModel, ContextModel>();
            RegisterAppStart<ShellViewModel, IContextModel>();           
        }
        public override IContextModel Startup(IContextModel parameter)
        {
            parameter = Mvx.IoCProvider.Resolve<IContextModel>();
            return parameter;
        }
    }
}
