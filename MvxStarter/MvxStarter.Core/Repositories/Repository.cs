﻿using MvxStarter.Core.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MvxStarter.Core.Repositories
{
    public class Repository<T> where T : IEntity
    {
        [JsonProperty]
        private List<T> _entities;

        public Repository()
        {

        }

        public Repository(List<T> entities)
        {
            _entities = entities;
        }

        public void Add(T entity)
        {
            _entities.Add(entity);
        }

        public void AddList(List<T> entities)
        {
            _entities.AddRange(entities);
        }

        public T GetEntityById(int id)
        {
            return _entities[id];
        }

        public List<T> GetAllEntities()
        {
            return _entities;
        }

        public void Delete(T entity)
        {
            _entities.Remove(entity);
        }

        public void DeleteList(List<T> entities)
        {
            _entities.RemoveAll(x => entities.Contains(x));
        }

        public void Save()
        {
            //_contextHelper.SaveDataType(_entities);
        }
    }
}
