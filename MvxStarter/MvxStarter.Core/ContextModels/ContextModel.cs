﻿using MvxStarter.Core.Models.BackGrounds;
using MvxStarter.Core.Models.Classes;
using MvxStarter.Core.Models.Enums;
using MvxStarter.Core.Models.Feats;
using MvxStarter.Core.Models.Items;
using MvxStarter.Core.Models.PC;
using MvxStarter.Core.Models.Proficiencies;
using MvxStarter.Core.Models.Races;
using MvxStarter.Core.Models.Spells;
using MvxStarter.Core.Repositories;
using MvxStarter.Core.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace MvxStarter.Core.ContextModels
{
    public class ContextModel : IContextModel
    {
        //public string FileFolderPath { get; set; }

        // public string FileName { get; set; }

        //public string FilePath => $"{FileFolderPath}\\{FileName}.json";
        public string FilePath { get; set; }
        public bool IsInitialized { get; set; } = false;
        

        [JsonProperty]
        public Repository<IPC> CharacterRepository { get; private set; }
        [JsonProperty]
        public Repository<IClass> ClassRepository { get; private set; }
        [JsonProperty]
        public Repository<IRace> RaceRepository { get; private set; }
        [JsonProperty]
        public Repository<ISpell> SpellRepository { get; private set; }
        [JsonProperty]
        public Repository<IItem> ItemRepository { get; private set; }
        [JsonProperty]
        public Repository<IFeat> FeatRepository { get; private set; }
        [JsonProperty]
        public Repository<IBackGround> BackGroundRepository { get; private set; }
        [JsonProperty]
        public Dictionary<ProficiencyType, IEnumerable<string>> ProficiencyDictionary { get; private set; } = GetProficienciesService.FillProficiencyList();

        public ContextModel()
        {
            
        }

        public ContextModel(string filePath,
                            Repository<IPC> characterRepository,
                            Repository<IClass> classRepository,
                            Repository<IRace> raceRepository,
                            Repository<ISpell> spellRepository,
                            Repository<IItem> itemRepository,
                            Repository<IFeat> featRepository,
                            Repository<IBackGround> backGroundRepository)
        {
            //FileFolderPath = fileFolderPath;
            //FileName = fileName;
            FilePath = filePath;
            CharacterRepository = characterRepository;
            ClassRepository = classRepository;
            RaceRepository = raceRepository;
            SpellRepository = spellRepository;
            ItemRepository = itemRepository;
            FeatRepository = featRepository;
            BackGroundRepository = backGroundRepository;

        }

        public void InitializeNewContextModel(string filePath)
        {
            FilePath = filePath;
            CharacterRepository = new Repository<IPC>(new List<IPC>());
            ClassRepository = new Repository<IClass>(new List<IClass>());
            RaceRepository = new Repository<IRace>(new List<IRace>());
            SpellRepository = new Repository<ISpell>(new List<ISpell>());
            ItemRepository = new Repository<IItem>(new List<IItem>());
            FeatRepository = new Repository<IFeat>(new List<IFeat>());
            BackGroundRepository = new Repository<IBackGround>(new List<IBackGround>());
        }

        public void SaveChanges()
        {
            //DELETE TO OVERRIDE
            if (File.Exists(FilePath))
            {
                File.Delete(FilePath);
            }
            //INIT CONVERTER
            JsonSerializer serializer = new JsonSerializer();
            //serializer.Converters.Add(new Newtonsoft.Json.Converters.JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;
            serializer.TypeNameHandling = TypeNameHandling.Auto;
            serializer.Formatting = Formatting.Indented;

            using (StreamWriter sw = new StreamWriter(FilePath, true))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, this, typeof(ContextModel));
            }            
        }

        public void OpenFile()
        {
            ContextModel currentContextModel = JsonConvert.DeserializeObject<ContextModel>(File.ReadAllText(FilePath), new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore,
            });            

            CharacterRepository = currentContextModel.CharacterRepository;
            ClassRepository = currentContextModel.ClassRepository;
            RaceRepository = currentContextModel.RaceRepository;
            SpellRepository = currentContextModel.SpellRepository;
            ItemRepository = currentContextModel.ItemRepository;
            FeatRepository = currentContextModel.FeatRepository;
            BackGroundRepository = currentContextModel.BackGroundRepository;
        }
    }
}
