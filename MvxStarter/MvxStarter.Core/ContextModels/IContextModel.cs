﻿using MvxStarter.Core.Models.BackGrounds;
using MvxStarter.Core.Models.Classes;
using MvxStarter.Core.Models.Enums;
using MvxStarter.Core.Models.Feats;
using MvxStarter.Core.Models.Items;
using MvxStarter.Core.Models.PC;
using MvxStarter.Core.Models.Races;
using MvxStarter.Core.Models.Spells;
using MvxStarter.Core.Repositories;
using System.Collections.Generic;

namespace MvxStarter.Core.ContextModels
{
    public interface IContextModel
    {
        //string FileFolderPath { get; set; }
        //string FileName { get; set; }
        string FilePath { get; set; }
        bool IsInitialized { get; set; }
        Repository<IPC> CharacterRepository { get; }
        Repository<IClass> ClassRepository { get; }
        Repository<IRace> RaceRepository { get; }
        Repository<ISpell> SpellRepository { get; }
        Repository<IItem> ItemRepository { get; }
        Repository<IFeat> FeatRepository { get; }
        Repository<IBackGround> BackGroundRepository { get; }
        Dictionary<ProficiencyType, IEnumerable<string>> ProficiencyDictionary { get; }
        void InitializeNewContextModel(string fileFolderPath);
        void OpenFile();
        void SaveChanges();
    }
}