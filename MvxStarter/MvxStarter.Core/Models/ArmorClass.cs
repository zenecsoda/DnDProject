﻿namespace MvxStarter.Core.Models
{
    public class ArmorClass
    {
        public int StaticAC { get; set; }
        public bool IsDexCompatible { get; set; }
        public int MaxDexBonus { get; set; }

        public ArmorClass()
        {
        }

        public ArmorClass(int staticAC, bool isDexCompatible, int maxDexBonus)
        {
            StaticAC = staticAC;
            IsDexCompatible = isDexCompatible;
            MaxDexBonus = maxDexBonus;
        }
    }
}
