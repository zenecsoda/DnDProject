﻿using MvxStarter.Core.Models.Enums;

namespace MvxStarter.Core.Models.Proficiencies
{
    public interface IToolProficiency:IProficiency
    {
        ToolType ToolType { get; set; }
        string Description { get; set; }
    }
}
