﻿using MvxStarter.Core.Models.Enums;

namespace MvxStarter.Core.Models.Proficiencies
{
    public class ToolProficiencyModel : IToolProficiency
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ProficiencyType Type { get; set; }
        public ToolType ToolType { get; set; }
    }
}
