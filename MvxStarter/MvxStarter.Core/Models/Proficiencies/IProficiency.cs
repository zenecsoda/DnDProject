﻿using MvxStarter.Core.Models.Enums;

namespace MvxStarter.Core.Models.Proficiencies
{
    public interface IProficiency:IDndObject
    {
        ProficiencyType Type { get; set; }
    }
}
