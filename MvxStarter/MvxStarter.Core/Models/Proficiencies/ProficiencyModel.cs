﻿using MvxStarter.Core.Models.Enums;

namespace MvxStarter.Core.Models.Proficiencies
{
    public class ProficiencyModel : IProficiency
    {
        public string Name { get; set; }
        public ProficiencyType Type { get; set; }
        
    }
}
