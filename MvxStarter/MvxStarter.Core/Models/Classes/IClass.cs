﻿using MvxStarter.Core.Models.PC.PcAtributes;
using MvxStarter.Core.Models.Proficiencies;
using System.Collections.Generic;

namespace MvxStarter.Core.Models.Classes
{
    public interface IClass:IDndObject, IEntity
    {
        //HP moddifers
        string HitDice { get; set; }
        string HitPointsAt1stLvl { get; set; }
        string HitPointsatHigherLvl { get; set; }
        //proficiency Moddifers
        IEnumerable<IProficiency> Proficiencies { get; set; } //armor, weapon, tool, language,skill
        ISavingThrows SavingThrows { get; set; }
        //equipments
        //classból jön...
        int ClassLvl { get; set; }
        IEnumerable<IClassLvlRecord> ClassLvlRecords { get; set; }





        string Description { get; set; }

    }
}
