﻿using System.Collections.Generic;

namespace MvxStarter.Core.Models.Classes
{
    public interface IClassLvlRecord
    {
        int Lvl { get; set; }
        int ProficiencyBonus { get; set; }
        IEnumerable<IClassFeature> ClassFeatures { get; set; }
        
    }
}
