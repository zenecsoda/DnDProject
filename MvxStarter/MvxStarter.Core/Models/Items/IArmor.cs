﻿using MvxStarter.Core.Models.Enums;

namespace MvxStarter.Core.Models.Items
{
    public interface IArmor : IItem
    {
        ArmorType ArmorType { get; set; }
        ArmorClass ArmorClass { get; set; }
        string StrengthRequirement { get; set; }
        bool StealthDisadvantage { get; set; }
    }
}
