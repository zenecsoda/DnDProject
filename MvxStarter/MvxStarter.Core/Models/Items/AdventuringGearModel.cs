﻿using MvxStarter.Core.Models.Enums;
using Newtonsoft.Json;

namespace MvxStarter.Core.Models.Items
{
    public class AdventuringGearModel : IAdventuringGear
    {

        [JsonProperty]
        private static int _nextId = 1;
        [JsonProperty]
        public int Id { get; set; }
        public string Name { get; set; }        
        public RarityType Rarity { get; set; }
        public string Cost { get; set; }
        public string Weight { get; set; }
        public string Description { get; set; }
        public ItemType Type { get; set; }
        public string Modifer { get; set; }
        public bool IsActive { get; set; }


        public AdventuringGearModel()
        {

        }

        public AdventuringGearModel(string name, RarityType rarity, string cost, string weight, string description, ItemType type)
        {
            Id = _nextId;
            _nextId++;

            Name = name;
            Rarity = rarity;
            Cost = cost;
            Weight = weight;
            Description = description;
            Type = type;
        }
    }
}
