﻿using MvxStarter.Core.Models.Enums;
using System.Collections.Generic;

namespace MvxStarter.Core.Models.Items
{
    public interface IWeapon : IItem
    {
        string Damamge { get; set; }
        DamageType DamageType { get; set; }
        IEnumerable<string> Properties { get; set; }
    }
}
