﻿using MvxStarter.Core.Models.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MvxStarter.Core.Models.Items
{
    public class WeaponModel : IWeapon
    {
        [JsonProperty]
        private static int _nextId = 1;

        [JsonProperty]
        public int Id { get; set; }
        public string Name { get; set; }
        public RarityType Rarity { get; set; }
        public string Cost { get; set; }
        public string Weight { get; set; }
        public string Description { get; set; }
        public ItemType Type { get; set; }
        public string Damamge { get; set; }
        public DamageType DamageType { get; set; }
        public IEnumerable<string> Properties { get; set; }            
        public string Modifer { get; set; }
        public bool IsActive { get; set; }

        public WeaponModel()
        {

        }

        public WeaponModel(string name, RarityType rarity, string cost, string weight, string description, ItemType type, string damamge, DamageType damageType, IEnumerable<string> properties)
        {
            Id = _nextId;
            _nextId++;

            Name = name;
            Rarity = rarity;
            Cost = cost;
            Weight = weight;
            Description = description;
            Type = type;
            Damamge = damamge;
            DamageType = damageType;
            Properties = properties;
        }
    }
}
