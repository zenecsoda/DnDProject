﻿using MvxStarter.Core.Models.Enums;

namespace MvxStarter.Core.Models.Items
{
    public interface IItem: IEntity
    {
        RarityType Rarity { get; set; }
        string Cost { get; set; }
        string Weight { get; set; }
        ItemType Type { get; set; }
        string Description { get; set; }
        string Modifer { get; set; }
        bool IsActive { get; set; }
    }
}
