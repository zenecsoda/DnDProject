﻿using MvxStarter.Core.Models.Enums;
using Newtonsoft.Json;

namespace MvxStarter.Core.Models.Items
{
    public class ArmorModel : IArmor
    {

        [JsonProperty]
        private static int _nextId = 1;
        [JsonProperty]
        public int Id { get; set; }
        public string Name { get; set; }        
        public RarityType Rarity { get; set; }
        public string Cost { get; set; }
        public string Weight { get; set; }
        public string Description { get; set; }
        public ItemType Type { get; set; }
        public ArmorType ArmorType { get; set; }
        public ArmorClass ArmorClass { get; set; }
        public string StrengthRequirement { get; set; }
        public bool StealthDisadvantage { get; set; }
        public string Modifer { get; set; }
        public bool IsActive { get; set; }


        public ArmorModel()
        {
        }

        public ArmorModel(string name,
                          RarityType rarity,
                          string cost,
                          string weight,
                          string description,
                          ItemType type,
                          ArmorType armorType,
                          ArmorClass armorClass,
                          string strengthRequirement,
                          bool stealthDisadvantage)
        {
            Id = _nextId;
            _nextId++;

            Name = name;
            Rarity = rarity;
            Cost = cost;
            Weight = weight;
            Description = description;
            Type = type;
            ArmorType = armorType;
            ArmorClass = armorClass;
            StrengthRequirement = strengthRequirement;
            StealthDisadvantage = stealthDisadvantage;
        }
    }
}
