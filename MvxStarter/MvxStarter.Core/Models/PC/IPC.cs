﻿using MvxStarter.Core.Models.BackGrounds;
using MvxStarter.Core.Models.Classes;
using MvxStarter.Core.Models.Feats;
using MvxStarter.Core.Models.Items;
using MvxStarter.Core.Models.PC.PcAtributes;
using MvxStarter.Core.Models.Proficiencies;
using MvxStarter.Core.Models.Races;
using MvxStarter.Core.Models.Spells;
using System;
using System.Collections.Generic;
using System.Text;

namespace MvxStarter.Core.Models.PC
{
    public interface IPC :  IDndObject, IEntity
    {


        IRace Race { get; set; }
        IClass Class { get; set; }
        int Lvl { get; set; }
        
        IBackGround BackGround { get; set; }
       
        IEnumerable<ISpell> Spells { get; set; }
        IEnumerable<IFeat> Feats { get; set; }

        IAbilityScores AbilityScore { get; set; }

        bool Inspiration { get; set; }
        int ProficiencyBonus { get; set; }
        ISavingThrows SavingThrows  { get; set; }
        ISkills Skills { get; set; }
        int PassivePerception { get; set; }
        IEnumerable<IProficiency> Proficiencies { get; set; }

        int Ac { get; set; }
        int Initiave { get; set; }
        int Speed { get; set; }

        int MaxHp { get; set; }
        int CurrentHp { get; set; }
        int TemporaryHp { get; set; }
        int MaxHitDiceNumber { get; set; }
        int CurrentHitDiceNumber { get; set; }
        int DeathSaveSuccess { get; set; }
        int DeathSaveFail { get; set; }
        //IEnumerable<IAttacksNSpellCasting> AttacksNSpellCasting { get; set; }
        IEnumerable<IItem> Inventory { get; set; }

        string PersonalityTraits { get; set; }
        string Ideals { get; set; }
        string Bonds { get; set; }
        string Flaws { get; set; }



    }
}
