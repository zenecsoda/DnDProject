﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MvxStarter.Core.Models.PC.PcAtributes
{
    public interface IAbilityScores
    {
        int STR { get; set; }
        int DEX { get; set; }
        int CON { get; set; }
        int INT { get; set; }
        int WIS { get; set; }
        int CHA { get; set; }

    }
}
