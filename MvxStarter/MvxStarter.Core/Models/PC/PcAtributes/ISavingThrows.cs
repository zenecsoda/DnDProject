﻿namespace MvxStarter.Core.Models.PC.PcAtributes
{
    public interface ISavingThrows
    {
        int STR { get; set; }
        int DEX { get; set; }
        int CON { get; set; }
        int INT { get; set; }
        int WIS { get; set; }
        int CHA { get; set; }


    }
}
