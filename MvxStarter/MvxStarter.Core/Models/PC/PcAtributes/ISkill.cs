﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MvxStarter.Core.Models.PC.PcAtributes
{
    public interface ISkill:IDndObject
    {
        string ModifierType { get; set; }
        string Proficiency { get; set; }
    }
}
