﻿using MvxStarter.Core.Models.Enums;
using MvxStarter.Core.Models.Items;
using MvxStarter.Core.Models.Proficiencies;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MvxStarter.Core.Models.BackGrounds
{
    public class BackGroundModel : IBackGround
    {
        [JsonProperty]
        private static int _nextId = 1;

        [JsonProperty]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<SkillType> Skill { get; set; }
        public IEnumerable<IProficiency> Proficiencies { get; set; }
        public IEnumerable<IItem> Items { get; set; }
        public string Features { get; set; }

        public BackGroundModel()
        {
               
        }

        public BackGroundModel(string name,
                               string description,
                               IEnumerable<SkillType> skill,
                               IEnumerable<IProficiency> proficiencies,
                               IEnumerable<IItem> items,
                               string features)
        {
            Id = _nextId;
            _nextId++;

            Name = name;
            Description = description;
            Skill = skill;
            Proficiencies = proficiencies;
            Items = items;
            Features = features;
        }
    }
}
