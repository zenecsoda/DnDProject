﻿using MvxStarter.Core.Models.Enums;
using MvxStarter.Core.Models.Items;
using MvxStarter.Core.Models.Proficiencies;
using System.Collections.Generic;

namespace MvxStarter.Core.Models.BackGrounds
{
    public interface IBackGround : IEntity
    {
        string Description { get; set; }
        IEnumerable<SkillType> Skill { get; set; }
        IEnumerable<IProficiency> Proficiencies { get; set; }
        IEnumerable<IItem> Items { get; set; }
        string Features { get; set; }
        
    }
}
