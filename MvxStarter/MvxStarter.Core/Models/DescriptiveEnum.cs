﻿using MvxStarter.Core.Models.Enums;

namespace MvxStarter.Core.Models
{
    public class DescriptiveEnum
    {
        public DamageType Data { get;set; }
    }
}
