﻿using MvxStarter.Core.Models.PC.PcAtributes;
using MvxStarter.Core.Models.Proficiencies;
using MvxStarter.Core.Models.Spells;
using System.Collections.Generic;

namespace MvxStarter.Core.Models.Feats
{
    public interface IFeat:IEntity
    {
        string Prerequisite { get; set; }
        string Description { get; set; }
        IAbilityScores AbilityScoreIncrease { get; set; }
        IEnumerable<IProficiency> Proficiencies { get; set; }
        IEnumerable<ISpell> Spells { get; set; }
        ISkill Skill { get; set; }
        //AC Increase
        //HP increase
        //Initiative Increase
        //Save prof
        //movment speed increase
        
        
    }
}
