﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum RangeType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Melee")]
        Melee,
        [Description("Ranged")]
        Ranged
    }
}
