﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum ToolType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("ArtisanTool")]
        ArtisanTool,
        [Description("GamingSet")]
        GamingSet,
        [Description("MusicalInstrument")]
        MusicalInstrument,
        [Description("Vehicles")]
        Vehicles
    }
}
