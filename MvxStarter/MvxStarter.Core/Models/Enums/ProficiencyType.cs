﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum ProficiencyType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Skill")]
        Skill,
        [Description("Language")]
        Language,
        [Description("Weapon")]
        Weapon,
        [Description("Armor")]
        Armor,
        [Description("Tool")]
        Tool,
        [Description("Other")]
        Other
    }
}
