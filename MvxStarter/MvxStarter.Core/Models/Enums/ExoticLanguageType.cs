﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum ExoticLanguageType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Abyssal")]
        Abyssal,
        [Description("Celestial")]
        Celestial,
        [Description("Draconic")]
        Draconic,
        [Description("Deep Speech")]
        DeepSpeech,
        [Description("Infernal")]
        Infernal,
        [Description("Primordial")]
        Primordial,
        [Description("Sylvan")]
        Sylvan,
        [Description("Undercommon")]
        Undercommon
    }
}
