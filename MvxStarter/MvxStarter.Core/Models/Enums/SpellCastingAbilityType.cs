﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum SpellCastingAbilityType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("None")]
        None,
        [Description("Spell")]
        Spell,
        [Description("Strength")]
        Strength,
        [Description("Dexterity")]
        Dexterity,
        [Description("Constitution")]
        Constitution,
        [Description("Intelligence")]
        Intelligence,
        [Description("Wisdom")]
        Wisdom,
        [Description("Charisma")]
        Charisma
    }
}
