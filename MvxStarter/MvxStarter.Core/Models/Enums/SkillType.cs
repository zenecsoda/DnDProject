﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum SkillType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Acrobatics")]
        Acrobatics,
        [Description("Animal Handling")]
        AnimalHandling,
        [Description("Arcana")]
        Arcana,
        [Description("Athletics")]
        Athletics,
        [Description("Deception")]
        Deception,
        [Description("History")]
        History,
        [Description("Insight")]
        Insight,
        [Description("Intimidation")]
        Intimidation,
        [Description("Investigation")]
        Investigation,
        [Description("Medicine")]
        Medicine,
        [Description("Nature")]
        Nature,
        [Description("Perception")]
        Perception,
        [Description("Performance")]
        Performance,
        [Description("Persuasion")]
        Persuasion,
        [Description("Religion")]
        Religion,
        [Description("Sleight of Hand")]
        SleightOfHand,
        [Description("Stealth")]
        Stealth,
        [Description("Survival")]
        Survival
    }
}
