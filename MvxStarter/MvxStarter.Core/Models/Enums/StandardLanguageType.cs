﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum StandardLanguageType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Common")]
        Common,
        [Description("Dwarvish")]
        Dwarvish,
        [Description("Elvish")]
        Elvish,
        [Description("Giant")]
        Giant,
        [Description("Gnomish")]
        Gnomish,
        [Description("Goblin")]
        Goblin,
        [Description("Halfling")]
        Halfling,
        [Description("Orc")]
        Orc
    }
}
