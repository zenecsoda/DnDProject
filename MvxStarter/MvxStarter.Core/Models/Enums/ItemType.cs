﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum ItemType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Weapon")]
        Weapon,
        [Description("Armor")]
        Armor,
        [Description("AdventureingGear")]
        AdventureingGear
    }
}
