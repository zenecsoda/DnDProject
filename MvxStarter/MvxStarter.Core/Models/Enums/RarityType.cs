﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum RarityType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Common")]
        Common,
        [Description("Uncommon")]
        Uncommon,
        [Description("Rare")]
        Rare,
        [Description("VeryRare")]
        VeryRare,
        [Description("Legendary")]
        Legendary
    }
}
