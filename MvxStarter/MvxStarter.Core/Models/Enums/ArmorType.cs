﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum ArmorType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Light")]
        Light,
        [Description("Medium")]
        Medium,
        [Description("Heavy")]
        Heavy,
        [Description("Shield")]
        Shield
    }
}
