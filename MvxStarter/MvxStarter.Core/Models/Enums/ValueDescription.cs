﻿namespace MvxStarter.Core.Models.Enums
{
    public class ValueDescription
    {
        public object Value { get; set; }
        public object Description { get; set; }
    }
}
