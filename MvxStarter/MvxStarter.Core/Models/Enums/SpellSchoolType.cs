﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum SpellSchoolType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Abjuration")]
        Abjuration,
        [Description("Conjuration")]
        Conjuration,
        [Description("Divination")]
        Divination,
        [Description("Enchantment")]
        Enchantment,
        [Description("Evocation")]
        Evocation,
        [Description("Illusion")]
        Illusion,
        [Description("Necromancy")]
        Necromancy,
        [Description("Transmutation")]
        Transmutation
    }
}
