﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum DamageType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("Piercing")]
        Piercing,
        [Description("Slashing")]
        Slashing,
        [Description("Bludgeoning")]
        Bludgeoning,
        [Description("Fire")]
        Fire,
        [Description("Cold")]
        Cold,
        [Description("Thunder")]
        Thunder,
        [Description("Lightning")]
        Lightning,
        [Description("Acid")]
        Acid,
        [Description("Poison")]
        Poison,
        [Description("Psychonic")]
        Psychonic,
        [Description("Force")]
        Force,
        [Description("Necrotic")]
        Necrotic,
        [Description("Radiant")]
        Radiant
    }
}
