﻿using System.ComponentModel;

namespace MvxStarter.Core.Models.Enums
{
    public enum SpellType
    {
        [Description("")]
        NOT_SET = 0,
        [Description("SpellCard")]
        SpellCard,
        [Description("SpellAttack")]
        SpellAttack
    }
}
