﻿namespace MvxStarter.Core.Models
{
    public interface IEntity:IDndObject
    {
        int Id { get; set; }
    }
}
