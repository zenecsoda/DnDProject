﻿using MvxStarter.Core.Models.PC.PcAtributes;
using MvxStarter.Core.Models.Proficiencies;
using System.Collections.Generic;

namespace MvxStarter.Core.Models.Races
{
    public interface IRace:IDndObject, IEntity
    {
        
        IAbilityScores AbilitiyScoreIncrease { get; set; }

        string Age { get; set; }
        string Alignment { get; set; }
        string Size { get; set; }
        int Speed { get; set; }
        //extra moddiferek
        int Darkvision { get; set; }

        IEnumerable<string> ExtraFeatures { get; set; }

        IEnumerable<IProficiency> Proficinecies { get; set; }

        IEnumerable<ISubRace> Subraces { get; set; }


    }
}
