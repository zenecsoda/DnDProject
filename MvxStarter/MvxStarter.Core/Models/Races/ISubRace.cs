﻿using MvxStarter.Core.Models.PC.PcAtributes;
using MvxStarter.Core.Models.Proficiencies;
using System.Collections.Generic;

namespace MvxStarter.Core.Models.Races
{
    public interface ISubRace
    {
        IAbilityScores AbilitiyScoreIncrease { get; set; }
        IEnumerable<string> ExtraFeatures { get; set; }
        IEnumerable<IProficiency> Proficiences { get; set; }
    }
}
