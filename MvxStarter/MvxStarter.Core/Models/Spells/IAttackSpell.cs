﻿using MvxStarter.Core.Models.Enums;
using System.Collections.Generic;

namespace MvxStarter.Core.Models.Spells
{
    public interface IAttackSpell:ISpell
    {
        RangeType RangeType { get; set; }
        IEnumerable<string> Damage { get; set; }
        IEnumerable<DamageType> DamageType { get; set; }
        string Healing { get; set; }
        bool AddAbilityModToDamageOrHealing { get; set; }
        SpellSaveType SavingThrow { get; set; }
        string SavingThrowEffect { get; set; }
        //TODO string HigherLvlCast { get; set; }
    }
}
