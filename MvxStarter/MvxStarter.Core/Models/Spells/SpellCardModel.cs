﻿using MvxStarter.Core.Models.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MvxStarter.Core.Models.Spells
{
    public class SpellCardModel : ISpell
    {
        [JsonProperty]
        private static int _nextId = 1;

        [JsonProperty]
        public int Id { get; set; }
        public string Name { get; set; }
        public SpellSchoolType School { get; set; }
        public bool Ritual { get; set; }
        public string CastingTime { get; set; }
        public string Range { get; set; }
        public string Target { get; set; }
        public bool Verbal { get; set; }
        public bool Somatic { get; set; }
        public bool Material { get; set; }
        public string MaterialDescription { get; set; }
        public bool Concentration { get; set; }
        public string Duration { get; set; }
        public SpellCastingAbilityType SpellCastingAbility { get; set; }
        public string Innate { get; set; }
        public string Description { get; set; }
        public SpellType SpellType { get; set; }

        public SpellCardModel()
        {

        }

        public SpellCardModel(string name,
                          SpellSchoolType school,
                          bool ritual,
                          string castingTime,
                          string range,
                          string target,
                          bool verbal,
                          bool somatic,
                          bool material,
                          string materialDescription,
                          bool concentration,
                          string duration,
                          SpellCastingAbilityType spellCastingAbility,
                          string innate,
                          string description,
                          SpellType spellType )
        {
            Id = _nextId;
            _nextId++;

            Name = name;
            School = school;
            Ritual = ritual;
            CastingTime = castingTime;
            Range = range;
            Target = target;
            Verbal = verbal;
            Somatic = somatic;
            Material = material;
            MaterialDescription = materialDescription;
            Concentration = concentration;
            Duration = duration;
            SpellCastingAbility = spellCastingAbility;
            Innate = innate;
            Description = description;
            SpellType = spellType;
        }
    }
}
