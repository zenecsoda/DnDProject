﻿using MvxStarter.Core.Models.Enums;

namespace MvxStarter.Core.Models.Spells
{
    public interface ISpell: IEntity
    {

        SpellSchoolType School { get; set; }
        bool Ritual { get; set; }
        string CastingTime { get; set; }
        string Range { get; set; }
        string Target { get; set; }
        bool Verbal { get; set; }
        bool Somatic { get; set; }
        bool Material { get; set; }
        string MaterialDescription { get; set; }
        bool Concentration { get; set; }
        string Duration { get; set; }
        SpellCastingAbilityType SpellCastingAbility { get; set; }
        string Innate { get; set; }
        string Description { get; set; }
        SpellType SpellType { get; set; }

    }
}
