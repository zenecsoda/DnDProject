﻿using MvxStarter.Core.Models.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MvxStarter.Core.Models.Spells
{
    public class AttackSpellModel:IAttackSpell
    {
        [JsonProperty]
        private static int _nextId = 1;

        [JsonProperty]
        public int Id { get; set; }
        public string Name { get; set; }
        public SpellSchoolType School { get; set; }
        public bool Ritual { get; set; }
        public string CastingTime { get; set; }
        public string Range { get; set; }
        public string Target { get; set; }
        public bool Verbal { get; set; }
        public bool Somatic { get; set; }
        public bool Material { get; set; }
        public string MaterialDescription { get; set; }
        public bool Concentration { get; set; }
        public string Duration { get; set; }
        public SpellCastingAbilityType SpellCastingAbility { get; set; }
        public string Innate { get; set; }
        public string Description { get; set; }
        public SpellType SpellType { get; set; }
        public RangeType RangeType { get; set; }
        public IEnumerable<string> Damage { get; set; }
        public IEnumerable<DamageType> DamageType { get; set; }
        public string Healing { get; set; }
        public bool AddAbilityModToDamageOrHealing { get; set; }
        public SpellSaveType SavingThrow { get; set; }
        public string SavingThrowEffect { get; set; }

        public AttackSpellModel()
        {

        }

        public AttackSpellModel(string name,
                                SpellSchoolType school,
                                bool ritual,
                                string castingTime,
                                string range,
                                string target,
                                bool verbal,
                                bool somatic,
                                bool material,
                                string materialDescription,
                                bool concentration,
                                string duration,
                                SpellCastingAbilityType spellCastingAbility,
                                string innate,
                                string description,
                                SpellType spellType,
                                RangeType rangeType,
                                IEnumerable<string> damage,
                                IEnumerable<DamageType> damageType,
                                string healing,
                                bool addAbilityModToDamageOrHealing,
                                SpellSaveType savingThrow,
                                string savingThrowEffect)        
        {
            Id = _nextId;
            _nextId++;

            Name = name;
            School = school;
            Ritual = ritual;
            CastingTime = castingTime;
            Range = range;
            Target = target;
            Verbal = verbal;
            Somatic = somatic;
            Material = material;
            MaterialDescription = materialDescription;
            Concentration = concentration;
            Duration = duration;
            SpellCastingAbility = spellCastingAbility;
            Innate = innate;
            Description = description;
            SpellType = spellType;
            RangeType = rangeType;
            Damage = damage;
            DamageType = damageType;
            Healing = healing;
            AddAbilityModToDamageOrHealing = addAbilityModToDamageOrHealing;
            SavingThrow = savingThrow;
            SavingThrowEffect = savingThrowEffect;
        }







    }
}
