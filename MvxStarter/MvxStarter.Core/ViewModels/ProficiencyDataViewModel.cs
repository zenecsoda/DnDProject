﻿using MvvmCross.ViewModels;
using MvxStarter.Core.Models.Enums;
using System.Collections.Generic;

namespace MvxStarter.Core.ViewModels
{
    public class ProficiencyDataViewModel : MvxViewModel
    {
        private ProficiencyType _proficiencyType;
        public ProficiencyType ProficiencyType
        {
            get { return _proficiencyType; }
            set
            {
                if (_proficiencyType != value)
                {
                    SetProperty(ref _proficiencyType, value);
                }
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
            }
        }

        public Dictionary<ProficiencyType, IEnumerable<string>> ProficiencyDictionary { get; set; }

        public ProficiencyDataViewModel()
        {

        }

        public ProficiencyDataViewModel(ProficiencyType proficiencyType, string name)
        {
            ProficiencyType = proficiencyType;
            Name = name;
        }

        public ProficiencyDataViewModel(Dictionary<ProficiencyType, IEnumerable<string>> proficiencyDictionary)
        {
            ProficiencyDictionary = proficiencyDictionary;
        }
    }
}
