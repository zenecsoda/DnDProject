﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvxStarter.Core.ContextModels;
using MvxStarter.Core.Models;
using Ookii.Dialogs.Wpf;

namespace MvxStarter.Core.ViewModels
{
    public class ShellViewModel : MvxViewModel<IContextModel>
    {
        private readonly IMvxNavigationService _navigationService;
        private IContextModel _contextModel;
        public ShellViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            _contextModel = Mvx.IoCProvider.Resolve<IContextModel>();
            _contextModel.InitializeNewContextModel("");
            NavigateToSpellListCommand = new MvxAsyncCommand(() => _navigationService.Navigate<SpellListViewModel, IContextModel>(_contextModel));
            NavigateToItemListCommand = new MvxAsyncCommand(() => _navigationService.Navigate<ItemListViewModel, IContextModel>(_contextModel));
            NavigateToFeatListCommand = new MvxAsyncCommand(() => _navigationService.Navigate<FeatListViewModel, IContextModel>(_contextModel));
            CreateNewFileCommand = new MvxCommand(CreateNewFile);
            OpenFileCommand = new MvxCommand(OpenFile);
            SaveFileCommand = new MvxCommand(SaveFile);
            SaveFileAsCommand = new MvxCommand(SaveFileAs);
            //AddCharacterCommand = new MvxCommand(AddCharacter);
        }
        public override void Prepare(IContextModel contextModel)
        {
            _contextModel = contextModel;
            ContextModelInitialized = _contextModel.IsInitialized;
        }
        public IMvxAsyncCommand NavigateToSpellListCommand { get; private set; }
        public IMvxAsyncCommand NavigateToItemListCommand { get; private set; }
        public IMvxAsyncCommand NavigateToFeatListCommand { get; private set; }

        public IMvxCommand CreateNewFileCommand { get; private set; }
        public IMvxCommand OpenFileCommand { get; private set; }
        public IMvxCommand SaveFileCommand { get; private set; }
        public IMvxCommand SaveFileAsCommand { get; private set; }


        //public IMvxCommand AddCharacterCommand { get; set; }

        public void AddCharacter()
        {
            CharacterGeneratorModel characterGenerator = new CharacterGeneratorModel
            {
                FirstName = FirstName,
                LastName = LastName
            };

            FirstName = string.Empty;
            LastName = string.Empty;

            CharacterGenerators.Add(characterGenerator);
        }

        public void CreateNewFile()
        {
            var dialog = new VistaSaveFileDialog()
            {
                Filter = "JavaScript Object Notation Files (*.json)|*.json",
                DefaultExt = "json",
                AddExtension = true
            };
            if (dialog.ShowDialog().GetValueOrDefault())
            {

                _contextModel.InitializeNewContextModel(dialog.FileName);
                ContextModelInitialized = true;
            }
        }

        public void OpenFile()
        {
            var dialog = new VistaOpenFileDialog()
            {
                Filter = "JavaScript Object Notation Files (*.json)|*.json",
                DefaultExt = "json",
                AddExtension = true
            };
            var result = dialog.ShowDialog();

            if (result != null && result == true)
            {
                //Get the path of specified file
                _contextModel.FilePath = dialog.FileName;

                _contextModel.OpenFile();
                ContextModelInitialized = true;
            }

        }

        public void SaveFile()
        {
            _contextModel.SaveChanges();
        }

        public void SaveFileAs()
        {
            //INITIALIZE
            var dialog = new VistaSaveFileDialog
            {
                InitialDirectory = Path.GetDirectoryName(_contextModel.FilePath),
                RestoreDirectory = true,
                Filter = "JavaScript Object Notation Files (*.json)|*.json",
                DefaultExt = "json",
                AddExtension = true
        };
            //SHOW
            var result = dialog.ShowDialog();            

            if (result == null || result != true)
            {
                return;//TODO hiba üzenet a mentés sikertelenségéről
            }
            //Get the path of specified file
            _contextModel.FilePath = dialog.FileName;


            _contextModel.SaveChanges();
        }

        private bool _contextModelInitialized;
        public bool ContextModelInitialized
        {
            get
            {
                return _contextModelInitialized;
            }

            set
            {
                SetProperty(ref _contextModelInitialized, value);
                _contextModel.IsInitialized = value;
            }
        }

        public bool CanAddCharacter => FirstName?.Length > 0 && LastName?.Length > 0;

        private ObservableCollection<CharacterGeneratorModel> _characterGenerators = new ObservableCollection<CharacterGeneratorModel>();

        public ObservableCollection<CharacterGeneratorModel> CharacterGenerators
        {
            get { return _characterGenerators; }
            set { SetProperty(ref _characterGenerators, value); }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                SetProperty(ref _firstName, value);
                RaisePropertyChanged(() => FullName);
                RaisePropertyChanged(() => CanAddCharacter);
            }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set
            {
                SetProperty(ref _lastName, value);
                RaisePropertyChanged(() => FullName);
                RaisePropertyChanged(() => CanAddCharacter);
            }
        }

        public string FullName => $"{FirstName} {LastName}";
    }
}
