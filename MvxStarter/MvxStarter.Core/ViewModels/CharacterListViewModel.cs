﻿using MvvmCross.ViewModels;
using MvxStarter.Core.Models.PC;

namespace MvxStarter.Core.ViewModels
{
    public class CharacterListViewModel : MvxViewModel
    {
        public CharacterListViewModel()
        {

        }

        //PROPERTIES
        private MvxObservableCollection<IPC> _characterList = new MvxObservableCollection<IPC>();
        public MvxObservableCollection<IPC> CharacterList
        {
            get { return _characterList; }
            set { SetProperty(ref _characterList, value); }
        }
    }
}
