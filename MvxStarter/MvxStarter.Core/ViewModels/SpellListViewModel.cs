﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvxStarter.Core.ContextModels;
using MvxStarter.Core.Models.Spells;

namespace MvxStarter.Core.ViewModels
{
    public class SpellListViewModel : MvxViewModel<IContextModel>
    {

        private readonly IMvxNavigationService _navigationService;
        private IContextModel _contextModel;
        public SpellListViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            NavigateToCreateNewSpellCommand = new MvxAsyncCommand(() => _navigationService.Navigate<CreateNewSpellViewModel, IContextModel>(_contextModel));
            NavigateToMainMenuCommand = new MvxAsyncCommand(() => _navigationService.Navigate<ShellViewModel, IContextModel>(_contextModel));
        }

        public override void Prepare(IContextModel contextModel)
        {
            _contextModel = contextModel;
            SpellList = new MvxObservableCollection<ISpell>(_contextModel.SpellRepository.GetAllEntities());
        }

        public IMvxAsyncCommand NavigateToCreateNewSpellCommand { get; private set; }
        public IMvxAsyncCommand NavigateToMainMenuCommand { get; private set; }

        //PROPERTIES
        private MvxObservableCollection<ISpell> _spellList = new MvxObservableCollection<ISpell>();
        public MvxObservableCollection<ISpell> SpellList
        {
            get { return _spellList; }
            set { SetProperty(ref _spellList, value); }
        }

    }
}
