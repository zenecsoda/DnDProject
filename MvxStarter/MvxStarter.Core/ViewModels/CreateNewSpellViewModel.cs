﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvxStarter.Core.ContextModels;
using MvxStarter.Core.Models;
using MvxStarter.Core.Models.Enums;
using MvxStarter.Core.Models.Spells;
using System.Collections.Generic;

namespace MvxStarter.Core.ViewModels
{
    public class CreateNewSpellViewModel : MvxViewModel<IContextModel>
    {

        private readonly IMvxNavigationService _navigationService;
        private IContextModel _contextModel;
        public CreateNewSpellViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            AddSpellCommand = new MvxCommand(AddSpell);
        }

        public override void Prepare(IContextModel contextModel)
        {
            _contextModel = contextModel;
        }

        public IMvxCommand AddSpellCommand { get; private set; }

        public void AddSpell()
        {
            switch (SpellOutput)
            {
                case SpellType.NOT_SET:
                    return;
                case SpellType.SpellCard:
                    _contextModel.SpellRepository.Add(CreateNewSpellCard());
                    break;
                case SpellType.SpellAttack:
                    _contextModel.SpellRepository.Add(CreateNewAttackSpell());
                    break;
                default:
                    break;
            }

            _navigationService.Navigate<SpellListViewModel, IContextModel>(_contextModel);
        }

        private SpellCardModel CreateNewSpellCard()
        {
            //CREATE INSTANCE
            var NewSpellCard = new SpellCardModel(Name,SpellSchool,IsRitual,CastingTime,Range,Target,IsVerbal,IsSomatic,IsMaterial,MaterialDescription,IsConcentration,Duration,SpellCastingAbility,Innate,Description,SpellOutput);
            //RETURN INSTANCE
            return NewSpellCard;
        }

        private AttackSpellModel CreateNewAttackSpell()
        {
            //DATA CONVERSION
            var damage = new List<string>();
            foreach (var item in Damage)
            {
                damage.Add(item.Data);
            }
            var damageType = new List<DamageType>();
            foreach (var item in DamageType)
            {
                damageType.Add(item.Data);
            }
            //CREATE INSTANCE
            var NewAttackSpell = new AttackSpellModel(Name, SpellSchool, IsRitual, CastingTime, Range, Target, IsVerbal, IsSomatic, IsMaterial, MaterialDescription, IsConcentration, Duration, SpellCastingAbility, Innate, Description, SpellOutput,RangeType,damage,damageType,Healing,AddAbilityModToDamageOrHealing,SpellSave,Effect);
            //RETURN INSTANCE
            return NewAttackSpell;
        }

        //PROPERTIES
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
            }
        }

        private SpellSchoolType _spellSchool;
        public SpellSchoolType SpellSchool
        {
            get { return _spellSchool; }
            set
            {
                if (_spellSchool != value)
                {
                    SetProperty(ref _spellSchool, value);
                }
            }
        }
        private bool _isRitual;
        public bool IsRitual
        {
            get { return _isRitual; }
            set
            {
                SetProperty(ref _isRitual, value);
            }
        }

        private string _castingTime;
        public string CastingTime
        {
            get { return _castingTime; }
            set
            {
                SetProperty(ref _castingTime, value);
            }
        }
        private string _range;
        public string Range
        {
            get { return _range; }
            set
            {
                SetProperty(ref _range, value);
            }
        }
        private string _target;
        public string Target
        {
            get { return _target; }
            set
            {
                SetProperty(ref _target, value);
            }
        }
        private bool _isVerbal;
        public bool IsVerbal
        {
            get { return _isVerbal; }
            set
            {
                SetProperty(ref _isVerbal, value);
            }
        }
        private bool _isSomatic;
        public bool IsSomatic
        {
            get { return _isSomatic; }
            set
            {
                SetProperty(ref _isSomatic, value);
            }
        }
        private bool _isMaterial;
        public bool IsMaterial
        {
            get { return _isMaterial; }
            set
            {
                SetProperty(ref _isMaterial, value);
            }
        }
        private string _materialDescription;
        public string MaterialDescription
        {
            get { return _materialDescription; }
            set
            {
                SetProperty(ref _materialDescription, value);
            }
        }
        private bool _isConcentration;
        public bool IsConcentration
        {
            get { return _isConcentration; }
            set
            {
                SetProperty(ref _isConcentration, value);
            }
        }
        private string _duration;
        public string Duration
        {
            get { return _duration; }
            set
            {
                SetProperty(ref _duration, value);
            }
        }
        private SpellCastingAbilityType _spellCastingAbility;
        public SpellCastingAbilityType SpellCastingAbility
        {
            get { return _spellCastingAbility; }
            set
            {
                if (_spellCastingAbility != value)
                {
                    SetProperty(ref _spellCastingAbility, value);
                }
            }
        }
        private string _innate;
        public string Innate
        {
            get { return _innate; }
            set
            {
                SetProperty(ref _innate, value);
            }
        }
        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                SetProperty(ref _description, value);
            }
        }


        private SpellType _spellOutput;
        public SpellType SpellOutput
        {
            get { return _spellOutput; }
            set
            {
                if (_spellOutput != value)
                {
                    SetProperty(ref _spellOutput, value);
                    RaisePropertyChanged(() => IsAttackSpell);
                    RaisePropertyChanged(() => IsSpellCard);
                }
            }
        }
        public bool IsAttackSpell => SpellOutput == SpellType.SpellAttack;
        public bool IsSpellCard => SpellOutput == SpellType.SpellCard;

        //AttackSpell
        private RangeType _rangeType;
        public RangeType RangeType
        {
            get { return _rangeType; }
            set
            {
                if (_rangeType != value)
                {
                    SetProperty(ref _rangeType, value);
                }
            }
        }

        private MvxObservableCollection<DescriptiveData> _damage = new MvxObservableCollection<DescriptiveData>();
        public MvxObservableCollection<DescriptiveData> Damage
        {
            get { return _damage; }
            set
            {
                SetProperty(ref _damage, value);
            }
        }
        private MvxObservableCollection<DescriptiveEnum> _damageType = new MvxObservableCollection<DescriptiveEnum>();
        public MvxObservableCollection<DescriptiveEnum> DamageType
        {
            get { return _damageType; }
            set
            {
                if (_damageType != value)
                {
                    SetProperty(ref _damageType, value);
                }
            }
        }
        private string _healing;
        public string Healing
        {
            get { return _healing; }
            set
            {
                SetProperty(ref _healing, value);
            }
        }
        private bool _addAbilityModToDamageOrHealing;
        public bool AddAbilityModToDamageOrHealing
        {
            get { return _addAbilityModToDamageOrHealing; }
            set
            {
                SetProperty(ref _addAbilityModToDamageOrHealing, value);
            }
        }
        private SpellSaveType _spellSave;
        public SpellSaveType SpellSave
        {
            get { return _spellSave; }
            set
            {
                if (_spellSave != value)
                {
                    SetProperty(ref _spellSave, value);
                }
            }
        }
        private string _effect;
        public string Effect
        {
            get { return _effect; }
            set
            {
                SetProperty(ref _effect, value);
            }
        }
        //TODO High lvl cast







    }
}
