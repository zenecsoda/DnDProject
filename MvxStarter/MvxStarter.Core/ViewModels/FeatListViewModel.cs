﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvxStarter.Core.ContextModels;
using MvxStarter.Core.Models.Feats;

namespace MvxStarter.Core.ViewModels
{
    public class FeatListViewModel : MvxViewModel<IContextModel>
    {
        private readonly IMvxNavigationService _navigationService;
        private IContextModel _contextModel;
        public FeatListViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            NavigateToCreateNewFeatCommand = new MvxAsyncCommand(() => _navigationService.Navigate<CreateNewFeatViewModel, IContextModel>(_contextModel));
            NavigateToMainMenuCommand = new MvxAsyncCommand(() => _navigationService.Navigate<ShellViewModel, IContextModel>(_contextModel));
        }

        public override void Prepare(IContextModel contextModel)
        {
            _contextModel = contextModel;
            FeatList = new MvxObservableCollection<IFeat>(_contextModel.FeatRepository.GetAllEntities());
        }

        public IMvxAsyncCommand NavigateToCreateNewFeatCommand { get; private set; }
        public IMvxAsyncCommand NavigateToMainMenuCommand { get; private set; }
        //NavigateToMainMenuCommand

        //PROPERTIES
        private MvxObservableCollection<IFeat> _featList = new MvxObservableCollection<IFeat>();
        public MvxObservableCollection<IFeat> FeatList
        {
            get { return _featList; }
            set { SetProperty(ref _featList, value); }
        }
    }
}
