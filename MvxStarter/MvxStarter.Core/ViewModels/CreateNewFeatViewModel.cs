﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvxStarter.Core.ContextModels;
using MvxStarter.Core.Models.Enums;
using MvxStarter.Core.Models.Proficiencies;
using MvxStarter.Core.Models.Spells;
using System.Collections.Generic;
using System.Linq;

namespace MvxStarter.Core.ViewModels
{
    public class CreateNewFeatViewModel : MvxViewModel<IContextModel>
    {
        private readonly IMvxNavigationService _navigationService;
        private IContextModel _contextModel;
        public CreateNewFeatViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            AddFeatCommand = new MvxCommand(AddFeat);
            AddProficiencyCommand = new MvxCommand(AddProficiency);
            AddSpellCommand = new MvxCommand(AddSpell);
            RemoveSpellCommand = new MvxCommand(RemoveSpell);
        }

        public override void Prepare(IContextModel contextModel)
        {
            _contextModel = contextModel;
            SpellListFromContextModel = new MvxObservableCollection <ISpell> (_contextModel.SpellRepository.GetAllEntities());

            ProficiencyDictionary = _contextModel.ProficiencyDictionary;

        }

        public IMvxCommand AddFeatCommand { get; private set; }
        public IMvxCommand AddProficiencyCommand { get; private set; }
        public IMvxCommand AddSpellCommand { get; private set; }
        public IMvxCommand RemoveSpellCommand { get; private set; }

        public void AddFeat()
        {
            //DATA CONVERSION
            //var Properties = new List<string>();
            //foreach (var property in ItemProperties)
            //{
            //    Properties.Add(property.Data);
            //}
            ////CREATE INSTANCE
            //var NewWeapon = new WeaponModel(Name, Rarity, Cost, Weight, Description, Type, Damage, DamageType, Properties);
            //RETURN INSTANCE

            _navigationService.Navigate<ItemListViewModel, IContextModel>(_contextModel);
        }
        public void AddProficiency()
        {
            SelectedProficiencies.Add(new ProficiencyDataViewModel(ProficiencyDictionary));
        }

        public void AddSpell()
        {
            if (SelectedSpellToAdd == null)
            {
                return;
            }
            if (FeatSpells.Contains(SelectedSpellToAdd))
            {
                return;
            }
            FeatSpells.Add(SelectedSpellToAdd);
            SpellListFromContextModel.Remove(SelectedSpellToAdd);
        }
        public void RemoveSpell()
        {
            if (SelectedSpellToRemove == null)
            {
                return;
            }
            if (SpellListFromContextModel.Contains(SelectedSpellToRemove))
            {
                return;
            }
            SpellListFromContextModel.Add(SelectedSpellToRemove);
            FeatSpells.Remove(SelectedSpellToRemove);
        }

        //PROPERTIES
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
            }
        }

        private string _prerequisite;
        public string Prerequisite
        {
            get { return _prerequisite; }
            set
            {
                SetProperty(ref _prerequisite, value);
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                SetProperty(ref _description, value);
            }
        }
        //IAbilityScores
        private bool _none;
        public bool None
        {
            get { return _none; }
            set
            {
                SetProperty(ref _none, value);
            }
        }
        private bool _str;
        public bool STR
        {
            get { return _str; }
            set
            {
                SetProperty(ref _str, value);
            }
        }
        private bool _dex;
        public bool DEX
        {
            get { return _dex; }
            set
            {
                SetProperty(ref _dex, value);
            }
        }
        private bool _con;
        public bool CON
        {
            get { return _con; }
            set
            {
                SetProperty(ref _con, value);
            }
        }
        private bool _int;
        public bool INT
        {
            get { return _int; }
            set
            {
                SetProperty(ref _int, value);
            }
        }
        private bool _wis;
        public bool WIS
        {
            get { return _wis; }
            set
            {
                SetProperty(ref _wis, value);
            }
        }
        private bool _cha;
        public bool CHA
        {
            get { return _cha; }
            set
            {
                SetProperty(ref _cha, value);
            }
        }

        private MvxObservableCollection<ISpell> _featSpells = new MvxObservableCollection<ISpell>();
        public MvxObservableCollection<ISpell> FeatSpells
        {
            get { return _featSpells; }
            set { SetProperty(ref _featSpells, value); }
        }

        private MvxObservableCollection<ISpell> _spellListFromContextModel = new MvxObservableCollection<ISpell>();
        public MvxObservableCollection<ISpell> SpellListFromContextModel
        {
            get { return _spellListFromContextModel; }
            set { SetProperty(ref _spellListFromContextModel, value); }
        }
        
        private ISpell _selectedSpellToAdd;
        public ISpell SelectedSpellToAdd
        {
            get { return _selectedSpellToAdd; }
            set
            {
                SetProperty(ref _selectedSpellToAdd, value);
                //TODO szepiteni
                if (value != null)
                {
                    SelectedSpellToRemove = null;
                }
            }
        }
        private ISpell _selectedSpellToRemove;
        public ISpell SelectedSpellToRemove
        {
            get { return _selectedSpellToRemove; }
            set
            {
                SetProperty(ref _selectedSpellToRemove, value);
                if (value != null)
                {
                    SelectedSpellToAdd = null;
                }
            }
        }
        


        private Dictionary<ProficiencyType, IEnumerable<string>> _proficiencyDicitonary;
        public Dictionary<ProficiencyType, IEnumerable<string>> ProficiencyDictionary
        {
            get { return _proficiencyDicitonary; }
            set { SetProperty(ref _proficiencyDicitonary, value); }
        }

        private MvxObservableCollection<ProficiencyDataViewModel> _selectedproficiencies = new MvxObservableCollection<ProficiencyDataViewModel>();
        public MvxObservableCollection<ProficiencyDataViewModel> SelectedProficiencies
        {
            get => _selectedproficiencies;
            set => SetProperty(ref _selectedproficiencies, value);
        }


    }
}
