﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvxStarter.Core.ContextModels;
using MvxStarter.Core.Models.Items;

namespace MvxStarter.Core.ViewModels
{
    public class ItemListViewModel : MvxViewModel<IContextModel>
    {
        private readonly IMvxNavigationService _navigationService;
        private IContextModel _contextModel;
        public ItemListViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            NavigateToCreateNewItemCommand = new MvxAsyncCommand(() => _navigationService.Navigate<CreateNewItemViewModel, IContextModel>(_contextModel));
            NavigateToMainMenuCommand = new MvxAsyncCommand(() => _navigationService.Navigate<ShellViewModel, IContextModel>(_contextModel));
        }

        public override void Prepare(IContextModel contextModel)
        {
            _contextModel = contextModel;
            ItemList = new MvxObservableCollection<IItem>(_contextModel.ItemRepository.GetAllEntities());
        }

        public IMvxAsyncCommand NavigateToCreateNewItemCommand { get; private set; }
        public IMvxAsyncCommand NavigateToMainMenuCommand { get; private set; }

        //PROPERTIES
        private MvxObservableCollection<IItem> _itemList = new MvxObservableCollection<IItem>();
        public MvxObservableCollection<IItem> ItemList
        {
            get { return _itemList; }
            set { SetProperty(ref _itemList, value); }
        }
    }
}
