﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvxStarter.Core.ContextModels;
using MvxStarter.Core.Models;
using MvxStarter.Core.Models.Enums;
using MvxStarter.Core.Models.Items;
using System;
using System.Collections.Generic;

namespace MvxStarter.Core.ViewModels
{
    public class CreateNewItemViewModel : MvxViewModel<IContextModel>
    {
        private readonly IMvxNavigationService _navigationService;
        private IContextModel _contextModel;
        public CreateNewItemViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            AddItemCommand = new MvxCommand(AddItem);
        }

        public override void Prepare(IContextModel contextModel)
        {
            _contextModel = contextModel;
        }

        public IMvxCommand AddItemCommand { get; private set; }

        public void AddItem()
        {
            switch (Type)
            {
                case ItemType.NOT_SET:
                    return;
                case ItemType.Weapon:
                    _contextModel.ItemRepository.Add(CreateNewWeapon());
                    break;
                case ItemType.Armor:
                    _contextModel.ItemRepository.Add(CreateNewArmor());
                    break;
                case ItemType.AdventureingGear:
                    _contextModel.ItemRepository.Add(CreateNewAdventuringGear());
                    break;
                default:
                    break;
            }

            _navigationService.Navigate<ItemListViewModel, IContextModel>(_contextModel);
        }

        private WeaponModel CreateNewWeapon()
        {
            //DATA CONVERSION
            var Properties = new List<string>();
            foreach (var property in ItemProperties)
            {
                Properties.Add(property.Data);
            }
            //CREATE INSTANCE
            var NewWeapon = new WeaponModel(Name, Rarity, Cost, Weight, Description, Type, Damage, DamageType, Properties);
            //RETURN INSTANCE
            return NewWeapon;
        }
        private ArmorModel CreateNewArmor()
        {
            //DATA CONVERSION
            var armorClass = new ArmorClass(StaticAc, IsDexCompatible, MaxDexBonus);
            //CREATE INSTANCE
            var NewArmor = new ArmorModel(Name, Rarity, Cost, Weight, Description, Type, ArmorType, armorClass, StrengthRequirement, StealthDisadvantage);
            //RETURN INSTANCE
            return NewArmor;
        }

        private AdventuringGearModel CreateNewAdventuringGear()
        {
            //CREATE INSTANCE
            var NewAdventuringGear = new AdventuringGearModel(Name, Rarity, Cost, Weight, Description, Type);
            //RETURN INSTANCE
            return NewAdventuringGear;
        }


        //PROPERTIES
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
            }
        }

        private RarityType _rarity;
        public RarityType Rarity
        {
            get { return _rarity; }
            set
            {
                if (_rarity != value)
                {
                    SetProperty(ref _rarity, value);
                }
            }
        }

        private string _cost;
        public string Cost
        {
            get { return _cost; }
            set
            {
                SetProperty(ref _cost, value);
            }
        }

        private string _weight;
        public string Weight
        {
            get { return _weight; }
            set
            {
                SetProperty(ref _weight, value);
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                SetProperty(ref _description, value);
            }
        }

        private ItemType _type;
        public ItemType Type
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    SetProperty(ref _type, value);
                    RaisePropertyChanged(() => IsTypeOfWeapon);
                    RaisePropertyChanged(() => IsTypeOfArmor);
                    RaisePropertyChanged(() => IsTypeOfAdventuringGear);
                    RaisePropertyChanged(() => IsTypeSelected);
                }
            }
        }

        public bool IsTypeOfWeapon => Type == ItemType.Weapon;
        public bool IsTypeOfArmor => Type == ItemType.Armor;
        public bool IsTypeOfAdventuringGear => Type == ItemType.AdventureingGear;
        public bool IsTypeSelected => Type != ItemType.NOT_SET;

        //WEAPON
        private string _damage;
        public string Damage
        {
            get { return _damage; }
            set
            {
                SetProperty(ref _damage, value);
            }
        }

        private DamageType _damageType;
        public DamageType DamageType
        {
            get { return _damageType; }
            set
            {
                if (_damageType != value)
                {
                    SetProperty(ref _damageType, value);
                }
            }
        }

        //ARMOR
        private ArmorType _armorType;
        public ArmorType ArmorType
        {
            get { return _armorType; }
            set
            {
                if (_armorType != value)
                {
                    SetProperty(ref _armorType, value);
                }
            }
        }

        private int _staticAc;
        public int StaticAc
        {
            get { return _staticAc; }
            set
            {
                SetProperty(ref _staticAc, value);
            }
        }

        private bool _isDexCompatible;
        public bool IsDexCompatible
        {
            get { return _isDexCompatible; }
            set
            {
                SetProperty(ref _isDexCompatible, value);
            }
        }

        private bool _isDexLimited;
        public bool IsDexLimited
        {
            get { return _isDexLimited; }
            set
            {
                SetProperty(ref _isDexLimited, value);
            }
        }

        private int _maxDexBonus;
        public int MaxDexBonus
        {
            get { return _maxDexBonus; }
            set
            {
                SetProperty(ref _maxDexBonus, value);
            }
        }

        private string _strengthRequirement;
        public string StrengthRequirement
        {
            get { return _strengthRequirement; }
            set
            {
                SetProperty(ref _strengthRequirement, value);
            }
        }

        private bool _stealthDisadvantage;
        public bool StealthDisadvantage
        {
            get { return _stealthDisadvantage; }
            set
            {
                SetProperty(ref _stealthDisadvantage, value);
            }
        }

        //COMMON
        private MvxObservableCollection<DescriptiveData> _itemProperties = new MvxObservableCollection<DescriptiveData>();
        public MvxObservableCollection<DescriptiveData> ItemProperties
        {
            get { return _itemProperties; }
            set { SetProperty(ref _itemProperties, value); }
        }
    }
}
